'use strict'

window.addEventListener('load', function(){
    //Timers  
    function intervalo(){
        var tiempo = setInterval(() => { // setTimeOut se ejecuta una sola vez, setInterval se ejecuta cada cierto tiempo
            //console.log('set interval ejecutando');
            let encabezado = document.querySelector('h1');
            //console.log('Tamaño del encabezado',encabezado.style.fontSize);
            if (encabezado.style.fontSize == '50px') {
                encabezado.style.fontSize = '40px';
            } else {
                encabezado.style.fontSize = '50px';
            }
        }, 1500);
        return tiempo;
    }

    var tiempo = intervalo();

    var stop = document.getElementById('stop');

    stop.addEventListener('click', () =>{
        clearInterval(tiempo);
        alert('Se ha detenido setInterval');
    });

    var start = document.getElementById('play');
    start.addEventListener('click', () => {
        intervalo();
        alert('Se ha iniciado setInterval');
    });
});

