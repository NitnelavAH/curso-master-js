'use strict'

/*
    Formulario campos: Nombre, Apellidos y edad
    Boton de enviar el formulario: evento submit
    Mostrar datos por pantalla
    Tener un bton que al darle click nos muestra los datos actuales del formulario
 */
const descripciones = ['Nombre', 'Apellido Paterno', 'Apellido Materno', 'Edad'];

 window.addEventListener('load', function(){
    console.log('DOM Cargado');

    var formulario = document.querySelector("#formulario");
    var enviados = document.getElementById('enviados');
    enviados.style.display = 'none';
    formulario.addEventListener('submit', (event)=>{
        console.log('submit formulario');

        //formulario

        var nombre = document.querySelector('#nombre').value;
        var apellidop = document.querySelector('#apellidop').value;
        var apellidom = document.querySelector('#apellidom').value;
        var edad = document.querySelector('#edad').value;

        var datos_enviados = [nombre, apellidop, apellidom, edad];

        if (validarNombreyApellidos(datos_enviados)) {
            var lista = document.createElement('dl');
            for (let i in datos_enviados) {
               let descripcion = document.createElement('dt');
               descripcion.append(descripciones[i]);
               descripcion.style.fontSize = '1.4rem'; 
               lista.appendChild(descripcion);
               let elemento = document.createElement('dd');
               elemento.append(datos_enviados[i]);
               elemento.style.fontSize = '1.2rem';
               lista.appendChild(elemento);
            }
    
            enviados.appendChild(lista);
    
    
            enviados.style.display = 'block';
        }
       
    });

 });

 function validarNombreyApellidos(valores){
     let bandera = false;
     for(let i in valores){
         if(i<(valores.length-1)){
            if (valores[i] === null || valores[i].trim().length === 0) {
                alert('El '+ descripciones[i]+' no es valido');
                return false;
            } else {
                bandera = true;
            }
         }else{
             valores[i] = parseInt(valores[i]);
             if (typeof valores[i] !== 'number' || valores[i] < 0 || isNaN(valores[i])) {
                 alert('La '+descripciones[i]+' no es valida');
                 return false;
             } else {
                 bandera = true;
             }
         }
     }
     console.log(bandera);
     return bandera;
 }