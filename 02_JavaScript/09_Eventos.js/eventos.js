'use strict'

// Evento load -> sirva par poder poner la etiqueta script de html en el head y no al final del body

window.addEventListener('load',() => {

    var h1 = document.querySelector('#h1');

    
    // click
    var botonjs = document.querySelector("#botonjs");
    
    botonjs.addEventListener('click',() => {
            cambiarColor();
        }
    );
    
    // Mouse over
    
    h1.addEventListener('mouseover', () => h1.style.background = "gray");
    
    // Mouse out
    
    h1.addEventListener('mouseout', function(){
        h1.style.background = "white";
    });
    
    var input = document.getElementById('campo_nombre');
    
    // Focus (foco dentro del input)
    input.addEventListener('focus', (event) => {
        console.log('event focus input');
    
    });
    
    // Blur (fuera de foco del input)
    input.addEventListener('blur', (event) => {
        console.log('event blur input');
    });
    
    // Keydown
    
    input.addEventListener('keydown', (event) => {
        console.log('keydown pulsaste la tecla', String.fromCharCode(event.keyCode));
    });
    
    // Keypress
    
    input.addEventListener('keypress', (event) => {
        console.log('keypress tecla presionada', String.fromCharCode(event.keyCode));
    });
    
    // keyup
    
    input.addEventListener('keyup', (event) => {
        console.log('keyup tecla soltada', String.fromCharCode(event.keyCode));
    });
});// end load

function cambiarColor() {
    let bg = h1.style.background;
    bg = bg.split(' ');
    console.log(bg);   
    if (bg[0] == "red") {
        h1.style.background = "green";
    } else {
        h1.style.background = "red";
    }
} 


