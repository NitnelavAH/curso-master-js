'use strict'

//Fetch(ir a buscar, peticiones asincronas a un servidor) y peticiones a servicios / apis rest
/* 
    servicios rest prueba
    https://jsonplaceholder.typicode.com/
    https://reqres.in/
*/
var usuarios = [];
var lista = document.getElementById('listado');
var pinfo = document.getElementById('info');

getUsuarios()
    .then(data => data.json())//promesa de getUsuarios()
    .then(users => {//promesa fetch en getUsuarios() ajax
        usuarios = users;
        listadoUsuarios(usuarios);
        console.log(usuarios);
        return getInfo();
   })
   .then(data => { //promesa getinfo()
        console.log('info',data);
        mostrarInfo(data);
        return getUsuario();
   })
   .then(data => data.json()) // promesa getUsuario()
   .then(usuario => { // promesa fetch getUsuario() ajax
        mostrarUsuario(usuario);
   }).catch(err => { // capturar errores
       console.log('ocurrio un error',err);
    });

function getUsuarios(){
    return fetch('https://jsonplaceholder.typicode.com/users');
}

function getUsuario(){
    return fetch('https://jsonplaceholder.typicode.com/users/1');
}

function getInfo(){
    var alumno = {
        nombre: 'Valentin',
        edad: 23,
        universidad: 'UAEMex',
        ciudad: 'Toluca'
    };

    return new Promise((resolve, reject) =>{
        let alumnoString = '';
        setTimeout(()=>{
            alumnoString = JSON.stringify(alumno);
            if (typeof alumnoString !== 'string' || alumnoString === '') reject('error');
    
            resolve(alumnoString);
        },3000);
        

    });

}

function listadoUsuarios(usuarios){
    usuarios.map((user,i) => {
        let elemento = document.createElement('li');
        elemento.append(user.name);
        lista.appendChild(elemento);

        document.querySelector('#cargando').style.display = 'none';
    });
}

function mostrarUsuario(usuario){
    let parrafo = document.querySelector('#usuario');
    parrafo.append(usuario.name);
    document.querySelector('#cargandoUser').style.display = 'none';
}

function mostrarInfo(info){
    pinfo.append(info);
    document.querySelector('#cargandoInfo').style.display = 'none';
}