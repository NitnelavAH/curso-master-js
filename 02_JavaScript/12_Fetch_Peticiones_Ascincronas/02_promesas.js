'use strict'

//ejemplo 2


//sincrono
/* let x = 10;
 console.log('1. proceso iniciado...');

 setTimeout(() => {
    x = x * 3 + 2;
    console.log('2. proceso terminado');
 }, 2000);

 console.log('3. el resultado es: ', x); */

 //asincrono
 let y = 10;

 var p = new Promise((resolve) => {
    setTimeout(() => {
        y = y * 3 + 2;
        console.log('2. proceso terminado');
        resolve(y);
     }, 2000);
 });

 console.log('1. proceso iniciado...');

 p.then(res => {
    console.log('3. el resultado es: ', res);
 });