'use strict'

//ejemplo 1

let x = 10;

const p = new Promise((resolve, reject) => {
    if (x === 11) {
        resolve('si, x es 10(parametro 1)');
    } else {
        reject('no, x no es 10');
    }
});

p.then((data) => {
    //cuando se ejecuta el resolve(la promesa se cumple)
    console.log(data);
}).catch((err) => {
    //cuando se ejecuta el reject(la promesa no se cumple)
    console.log(err);
});