'use strict'

let usuarios = [{
    id: 1,
    nombre: 'Valentín'
},{
    id: 2,
    nombre: 'Jovanny'
}];

let telefonos = [{
    id: 1,
    telefono : 7221345263
},{
    id: 2,
    telefono: 7221352627
}];

const obtenerUsuario = id => {
    return new Promise((resolve, reject) => {
        if(usuarios.find(usuario => usuario.id === id)){
            resolve('El usuario Existe');
        }else{
            reject('El usuario no existe');
        }
    });
};

const obtenerTelefono = id => {
    return new Promise((resolve, reject) => {
        if(telefonos.find(telefono => telefono.id === id)){
            resolve('El telefono Existe');
        }else{
            reject('El telefono no existe');
        }
    });
};


obtenerUsuario(2)
    .then(res =>{ //promesa obtenerUsuario
        console.log(res);
        return obtenerTelefono(1);
    }).then(res=> { //promesa obtenertelefono
    console.log(res);
    }).catch(err => console.log(err)); //catch error