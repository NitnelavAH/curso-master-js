'use strict' //uso estricto

//operadores

var num1 = 10;
var num2 = 5;

var suma = num1 + num2;
console.log('la suma de '+num1+' y '+ num2 +' es: '+ suma);

var resta = num1 - num2;
console.log('la resta de '+num1+' y '+ num2 +' es: '+ resta);
 
var division = num1 / num2;
console.log('la division de '+num1+' y '+ num2 +' es: '+ division);
 
var multiplicacion = num1 * num2;
console.log('la multiplicacion de '+num1+' y '+ num2 +' es: '+ multiplicacion);

var resto = num1 % 2;
console.log(resto);
if(resto === 0){
    console.log(num1 + ' es par');
}

var resto2 = num2 % 2;
console.log(resto2);
if(resto2 !== 0){
    console.log(num2 + ' es impar');
}

// Tipos de datos
var numero_entero = 24;
console.log(numero_entero, 'soy un entero');
var cadena_texto = 'hola ":v"';
console.log(cadena_texto);
var cadena_texto2 = "hola ':v'";
console.log(cadena_texto2);
var boolean = true; // true o false
console.log('soy un booleano', boolean);

// Funciones conversion
var numeroFalso = '11.5';
var numeroVerdadero = 11.5;
console.log(Number(numeroFalso), 'funcion number');
console.log(parseFloat(numeroFalso), 'funcion parseFloat');
console.log(parseInt(numeroFalso), 'funcion parseInt');
console.log(String(numeroVerdadero), 'funcion String');
console.log(numeroVerdadero.toString(), 'funcion toString');
console.log(numeroVerdadero+'', 'concatenacion con string');

// TypeOf
var flotante = 10.5;
console.log('Typeof tipos de datos');
console.log(typeof boolean);
console.log(typeof cadena_texto);
console.log(typeof numeroVerdadero);


 