'use strict' //uso estricto

// Var funciona como variable global en todos los bloques de codigo
// define una variable global o local en una función sin importar el ámbito del bloque.
var numero = 40;
console.log(numero);//valor 40

if(true){
    var numero = 50;
    console.log(numero);//valor 50
}

console.log(numero);//valor 50

// Let funciona solo en los bloques de codigo
// let te permite declarar variables limitando su alcance (scope) al bloque
var texto = 'curso js';
console.log(texto); // valor curso js

if (true) {
    let texto = "curso ionic"; // se crea una nueva variable para el bloque de codigo
    console.log(texto); // valor curso ionic
}

console.log(texto); // valor curso js