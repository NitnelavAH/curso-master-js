'use strict'

//localstorage

//comprobar disponibilidad del localstorage
if (typeof(Storage) !== 'undefined') {
    console.log('LocalStorage Disponible');
} else{
    console.log('LocalStorage no Disponible');
}

//guardar datos
localStorage.setItem("titulo","curso master JS");

//recuperar elemento de local strorage
var local = localStorage.getItem('titulo');
console.log('Datos Storage',local);

//guardar objetos

var usuario = {
    nombre: 'Jose Valentin',
    email: 'nitnelav_ah@hotmail.com',
    web: 'valentinabundo.mx'
}

localStorage.setItem('user',JSON.stringify(usuario));

// recuperar objeto
var userJS = JSON.parse(localStorage.getItem('user'))
console.log(userJS);

//limpiar local storage
localStorage.removeItem('titulo');
localStorage.clear();