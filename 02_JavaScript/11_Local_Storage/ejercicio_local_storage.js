'use strict'

var caja = document.getElementById('cajaPeliculas');
if (localStorage.length === 0) {
    caja.style.display = 'none';
}

var formulario = document.getElementById('formulario');
var formularioEliminar = document.getElementById('formEliminar');



formulario.addEventListener('submit', () =>{
    let pelicula = document.querySelector('#pelicula').value;
    if (validarPelicula(pelicula)) {
        localStorage.setItem(pelicula, pelicula);
    } 
    
});

formularioEliminar.addEventListener('submit', () => {
    let pelicula = document.querySelector('#peliculaEliminar').value;
    if (validarPelicula(pelicula)) {
        localStorage.removeItem(pelicula);
    }
});

actualizarCaja();

function validarPelicula(pelicula) {
    console.log('validar');
    let bandera = false;
    if (pelicula.length === 0 || pelicula.trim().length === 0) {
        alert('Ingresa una Pelicula Valida');
    } else{
        bandera = true;
    }
    return bandera;
}

function actualizarCaja(){
    
    
    if (localStorage.length === 1) {
        caja.style.display = 'block';
    }
    for(let i in localStorage){
        
        let lista = document.getElementById('listaPeliculas');
        let elemento = document.createElement('li');
        if(typeof localStorage[i] === 'string'){
            elemento.append(localStorage[i]);
            lista.appendChild(elemento);
        }  
    }
}

