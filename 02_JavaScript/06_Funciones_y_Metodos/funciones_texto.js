'use strict'

//Transformacio de textos

var numero = 666;
var texto1 = "Curso JS Master";
var texto2 = "Estoy empezando en JS con el curso visto en UDEMY";
var vacio = "";

var numeroAString = numero.toString();
console.log(numeroAString, 'es', typeof numeroAString);
console.log(texto1, 'convertido a mayusculas', texto1.toUpperCase());
console.log(texto1, 'convertido a minusculas', texto1.toLowerCase(), '\n\n\n');

//Calcular Longitud
console.log('el tamaño de vacio es', vacio.length);
console.log('el tamaño de texto 2 es', texto2.length,'\n\n\n');

//Concatenar
console.log('textos concatenados', texto1.concat(' '+texto2),'\n\n\n');

//Busquedas
// si la palabra no se encuenta se regresa un valor -1
console.log('la primera aparicion de la palabra en aparece "en" la posicion(indexOf)', texto2.indexOf('en'));
console.log('la primera aparicion de la palabra en aparece "en" la posicion(search)', texto2.search('en'));
console.log('la ultima aparicion de la palabra en aparece "en" la posicion(lastIndexOf)', texto2.lastIndexOf('en'));

console.log('metodo match buscando la palabra "en"', texto2.match('en'));
console.log('metodo match buscando todas las "en"', texto2.match(/en/g), '\n\n\n');

// obtener palabras/letra de un string

console.log('obneter la palabra "empezando":', texto2.substr(6,9));//substr(posicion, numeroCaracteres)
console.log('obtener la letra en la posicion 4, la letra es:', texto2.charAt(4),'\n\n\n');

//buscar en el inicio de un string
console.log('la variable texto 1 empieza con "Curso JS"?',texto1.startsWith('Curso JS') );
console.log('la variable texto 1 empieza con "Curso PHP"?',texto1.startsWith('Curso PHP') );
console.log('la variable texto 1 empieza con "JS" en la posicion 6?',texto1.startsWith('JS',6) );
console.log('la variable texto 1 empieza con "PHP" en la posicion 6?',texto1.startsWith('PHP',6),'\n\n\n' );

//buscar al final del string
console.log('la variable texto 1 termina con "Master"?',texto1.endsWith('Master') );
console.log('la variable texto 1 termina con "Easy"?',texto1.endsWith('Easy'),'\n\n\n' );

//se encuentra la palabra ...
console.log('Existe la palabra "Master" en la cadena', texto1.includes('Master'));
console.log('Existe la palabra "Easy" en la cadena', texto1.includes('Easy'));

