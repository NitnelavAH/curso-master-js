'use strict'

//replace
var texto = 'Hola este es el curso Hola Master en JavaScript';
var espacios = '    Quitar espacios por delante y detras     ';

console.log(texto, '\n');

console.log('Reemplazar "Hola" por "Que tal"','\n\n', texto.replace(/Hola/g,'Que Tal'), '\n\n');

// slice separar string a partir del caracter indicado

console.log('Mostrar el texto a partir de la posicion 15','\n\n', texto.slice(15),'\n\n');

console.log('Mostrar el texto a partir de la posicion 15 y hasta la 33','\n\n', texto.slice(15, 33),'\n\n');

//split recortar la palabra en el caracter o string indicado y la almenena en una array
console.log('convertir palabra a array', texto.split(), '\n\n');
console.log('separar la palabra por espacios y guardar en array', texto.split(" "),'\n\n');

//trim 
console.log(espacios, '\n');
console.log('quitar espacios por delante y detras del string','\n\n', espacios.trim());