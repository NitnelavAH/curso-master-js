
var variableGlobal = 'Hola soy una variable global'; // se puede acceder desde cualquier lugar del documento

function holaMundo(texto) {
    var variableLocal = 'Hola soy una variable local'; // solo se puede acceder a ella dentro de la funcion donde se declaro
    
    console.log(texto);
    console.log(variableGlobal, 'desde la funcion holaMundo');
    console.log(variableLocal, 'desde la funcion holaMundo');

}

holaMundo('funcion holaMundo');
// console.log(variableLocal); Esto no se puede ya que la variable esta definida de forma local dentro de la funcion holaMundo


