'use strict'

// Funciones

holaMundo();

'use strict'

// Definicion de Funciones
function holaMundo() {
    console.log('Hola Mundo desde una funcion');
}

// Parametros

function calculadora(num1, num2, opcional = false){
    console.log('Suma: ',(num1 + num2));
    console.log('Resta: ',(num1 - num2));
    console.log('División: ',(num1 / num2));
    console.log('Multiplicación: ',(num1 * num2));

    if (opcional) {
        console.log('se mando el parametro opcional');
    } else {
        console.log('no se mando el parametro opcional');
    }
}

calculadora(12, 4);// sin parametro opcional
calculadora(12, 4, true); // con el parametro opcional