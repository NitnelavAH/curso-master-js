// En las funciones de flecha se sustituye la palabra reservad afunction por la felcha =>

function sumame(numero1, numero2, sumaYmuestra, sumaPorDos){
    var sumar = numero1 + numero2;

    sumaYmuestra(sumar);
    sumaPorDos(sumar);

    return sumar;
}

sumame(5, 5, (dato) => {
    console.log("La suma es: ", dato);
},
(dato) => {
    console.log("La suma x 2 es: ", dato*2);
});

