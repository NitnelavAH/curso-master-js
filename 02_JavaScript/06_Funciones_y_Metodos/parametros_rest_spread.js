'use strict'
// Parametros REST y SPREAD

function listadoAutos(auto1, auto2, ...restoDeAutos){
    console.log('Auto 1: ', auto1);
    console.log('Auto 2: ', auto2);
    console.log(restoDeAutos);
}

listadoAutos('VW Sedan','VW Saveiro', "VW Type 2", "VW Brazila", "VW Golf", "VW Caribe");

var restoAutos = [ "VW Type 2", "VW Brazila"];
listadoAutos(...restoAutos,'VW Sedan','VW Saveiro', "VW Golf", "VW Caribe" );