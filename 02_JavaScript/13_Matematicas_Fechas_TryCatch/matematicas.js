'use strict'

var x = Math.PI;            // Returns PI
var y = Math.sqrt(16);      // Returns the square root of 16 

console.log('EL valor de PI es:',x);
console.log('El valor de la raiz cuadrada de 16 es:',y);

// numero aleatorio entre 1 y 100
var random = Math.floor((Math.random() * 100) + 1);

console.log('nuemero aleatorio entre 1 y 100 = ', random);

var round = Math.round(2.5);

console.log('redondeo de 2.5 es', round);