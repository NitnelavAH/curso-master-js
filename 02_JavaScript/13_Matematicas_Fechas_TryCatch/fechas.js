'use strict'


var fecha = new Date();
var datos = [];
console.log(fecha);

datos.push('Año: ' + fecha.getFullYear());
datos.push('Mes: ' + fecha.getMonth());
datos.push('Dia: ' + fecha.getDate());
datos.push('Hora: ' + fecha.getHours());
datos.push('Minutos: '+ fecha.getMinutes());
obtenerDatos();

function obtenerDatos() {
    let lista = document.getElementById('datosFecha');
    datos.forEach(dato => {
        let elemento = document.createElement('li');
        elemento.append(dato);
        lista.appendChild(elemento);
    });
}