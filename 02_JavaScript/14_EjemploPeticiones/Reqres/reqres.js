'use strict'

const $url = 'https://reqres.in/';

//GET

getUser().then(response => {
    // console.log(response);
    response.json().then(data => {
        console.log('GET',data);
        document.getElementById('getName').append(data.data.first_name);
        document.getElementById('getLastName').append(data.data.last_name);
        document.getElementById('getEmail').append(data.data.email);
        document.getElementById('getAvatar').setAttribute('src', data.data.avatar)
    });
});

 function getUser(){
    return fetch($url+'api/users/2');
}

//POST
var user = {
    "name": "morpheus",
    "job": "leader"
};

fetch($url+'api/users',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify(user)
}).then((resp) => {
    console.log('POST',resp);
}).catch(err => console.log(err));

//PUT

fetch($url+'api/users/2',{
    method: 'PUT',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        name: 'Valentin',
        job: 'Nokia'
    })
}).then((resp) => {
    console.log('PUT',resp);
}).catch(err => console.log(err));

//DELETE
fetch($url+'api/users/2',{
    method: 'DELETE',
    headers: {
        'Content-Type': 'application/json',
    }
}).then((resp) => {
    console.log('DELETE',resp);
}).catch(err => console.log(err));