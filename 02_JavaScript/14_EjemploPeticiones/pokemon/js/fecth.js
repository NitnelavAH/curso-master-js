'use strict'

const $pokemon = document.querySelector('#pokemon');
const $pokemon2 = document.getElementById('pokemon2');
const $pokemon3 = document.getElementById('pokemon3');

function renderPokemon(image, pokemon){
    pokemon.setAttribute('src', image);
}


/* 
    POR DEFECTO ES GET
    fetch('url',{
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        name: 'holamundo',
        age: 2020
    })
}); */


//GET
fetch('/JavaScript/14_EjemploPeticiones/pokemon/data/pokemon.json').then(resp => {
    // console.log(resp);
    resp.json().then(data => {
        console.log(data);
        renderPokemon(data.image, $pokemon);
    }).catch( error =>{
        console.log(error);
    });
}).catch(err => {
    console.log(err);
});


//GET image y convertirla
fetch('/JavaScript/14_EjemploPeticiones/pokemon/images/pikachu.png').then(resp =>{
    // console.log(resp);
    resp.blob().then((binaryLargeObject) => {
        console.log(binaryLargeObject);
        const domString = URL.createObjectURL(binaryLargeObject);
        console.log(domString);
        renderPokemon(domString, $pokemon2);
    });
}).catch((err) =>{
    console.log(err);
});

//Get api

fetch('https://pokeapi.co/api/v2/pokemon/25').then(resp => {
    resp.json().then(data => {
        console.log(data);
        renderPokemon(data.sprites.front_default, $pokemon3);
    });
}).catch( error => console.log(error));