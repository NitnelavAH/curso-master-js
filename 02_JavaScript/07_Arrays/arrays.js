'use strict'

var nombres = ["Juan", "Julio", "Jhon", "Perla", "Rosa", "Diana"];

var lenguajes = new Array("Java", "JavaScript", "C", "PHP", "Kotlin", "Dart");

console.log('Elemento 3 de nombres es:', nombres[3]);
console.log('Elemento 5 de lenguajes es:', lenguajes[5]);

document.writeln('<h2>Mostrar array con for</h2>');
for (let i = 0; i < lenguajes.length; i++) {
    document.writeln('<ul>');
   console.log('elemento', i, 'del arreglo lenguajes es:', lenguajes[i]);
   document.writeln('<li>','elemento ', i, ' del arreglo lenguajes es: ', lenguajes[i],'</li>');
   document.writeln('</ul>');
}

console.log('\n\n');

document.writeln('<h3>Mostrar array con foreach</h3>');

nombres.forEach((elemento, i) => {
    document.writeln('<ul>');
    console.log('elemento', i, 'del arreglo nombres es:', elemento);
    document.writeln('<li>','elemento ', i, ' del arreglo nombres es: ',elemento,'</li>');
    document.writeln('</ul>');
});

document.writeln('<h4>Mostrar array con forin</h4>');
for (let indice in lenguajes) {
    document.writeln('<ul>');
    document.writeln('<li>','elemento ', indice, ' del arreglo lenguajes es: ', lenguajes[indice],'</li>');
    document.writeln('</ul>');
}
/* 
for (let i = 0; i < nombres.length; i++) {
    document.writeln('<ul>');
    console.log('elemento', i, 'del arreglo nombres es:', nombres[i]);
    document.writeln('<li>','elemento ', i, ' del arreglo nombres es: s', nombres[i],'</li>');
    document.writeln('</ul>');
 } */