'use strict'

var peliculas = ['Batman', 'SpiderMan', 'Rapido y Furioso', 'Antman', 'Jurassic Park'];

console.log('arreglo original',peliculas);

// añadir elemento al final del array
peliculas.push('Cars');
console.log('elemento añadido', peliculas);

// quitar ultimo elemento del array
peliculas.pop();
console.log('ultimo elemento removido', peliculas);

//eliminar elemento del array

var indice = peliculas.indexOf('Antman');
console.log('Antman esta en la posicion', indice);
if (indice > -1) {
    peliculas.splice(3,1);//(posicion, elementos a eliminar desde esa posicion)
}
console.log('arreglo actualizado sin antman', peliculas);

//insertar al inicio del arreglo
peliculas.unshift('Hombre al agua');
console.log('Elemento añadido al inicio del arreglo', peliculas);

//convertir array a string, cada elemento se separa con comas o el separador que indiquemos
var peliculasString = peliculas.join(' <--> ');
console.log(peliculasString);

//convertir string a array, indicando por que caracter se va a separar
var stringToArray = peliculasString.split(' <--> ');
console.log(stringToArray);

//ordenar alfabeticamente
console.log('Array ordenado alfabeticamente', peliculas.sort());

//invertir array alfabeticamente
console.log('Array invertido en posiciones', peliculas.reverse());

// Busquedas

var buscar = peliculas.find(pelicula => pelicula == 'Jurassic Park'); // si lo encuentra regresa Jurassic Park si no regresa undefined
if (buscar) {
    console.log('se encontro ', buscar);
} else {
    console.log('no se encontro');
}

var indexBuscar = peliculas.findIndex(pelicula => pelicula == 'Jurassic Park');// regresa el indice donde se encontro
if (indexBuscar > -1) {
    console.log('Se encontro la busqueda en la posicion', indexBuscar);
} else {
    console.log('no se encontro');
}

var precios = [10,12,14,43,54,67,80,55];

var verificaPrecios = precios.some(precio => precio < 10);
console.log('hay precios menores que 10?', verificaPrecios);

