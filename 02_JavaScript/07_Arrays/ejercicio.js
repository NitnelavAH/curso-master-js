'use strict'

/* 
    pedir 6 numero por pantalla y gurdarlos en un array
    mostrar todos los elementos del array en el cuerpo de la pagina y la consola
    ordenar array y mostrarlo
    invertir orden y mostrarlo
    mostrar cuantos elementos tiene el array
    buscar un valor introducido por el usuario y decir si esta y su posicion
*/

var numeros = new Array();

do {
    let numero = parseInt(prompt('Ingresa el numero '+ (numeros.length + 1)));
    if (isNaN(numero)) {
        alert('Ingresa un numero valido');
    } else {
        numeros.push(numero);
        console.log('Se añadio el numero', numero);
    }
} while (numeros.length !== 6);

document.writeln('<p><strong>Elementos del array</strong></p><hr>');
document.writeln('<ol>');
numeros.forEach( (numero, index) => {
    console.log('numero en la posicion',index,'es',numero);
    document.writeln('<li>',numero,'</li>');
});

document.writeln('</ol>');

document.writeln('<p><strong>Array Ordenado</strong></p><hr>');
document.writeln('<ol>');
var numerosOrdenado = numeros.sort((a, b) => a - b);
for (let numero in numerosOrdenado) {
    document.writeln('<li>',numerosOrdenado[numero],'</li>');
}
document.writeln('</ol>');

document.writeln('<p><strong>Array Ordenado de manera inversa</strong></p><hr>');
document.writeln('<ol>');
var numerosOrdenado = numeros.sort((a, b) => a - b);
numerosOrdenado = numerosOrdenado.reverse();
for (let numero in numerosOrdenado) {
    document.writeln('<li>',numerosOrdenado[numero],'</li>');
}
document.writeln('</ol>');

document.writeln('<p><strong>El Array tiene ',numeros.length,' elementos</strong></p><hr>');


var buscar;


do {
    buscar = parseInt(prompt('Ingresa el numero a buscar'));
    if (isNaN(buscar)) {
        alert('Ingresa un numero valido');
    } 
} while (isNaN(buscar));

console.log('El numero a buscar es', buscar);
alert('El numero a buscar es '+ buscar);

var indexBuscar =numeros.findIndex(numero => numero == buscar);// regresa el indice donde se encontro
if (indexBuscar > -1) {
    console.log('Se encontro la busqueda del numero',buscar ,'en la posicion', indexBuscar);
    document.writeln('<p><strong>Se encontro la busqueda del numero',buscar ,'en la posicion', indexBuscar,'</strong></p><hr>');
} else {
    console.log('no se encontro el numero', buscar);
    document.writeln('<p><strong>No se encontro el numero ',buscar,'</strong></p><hr>');
}