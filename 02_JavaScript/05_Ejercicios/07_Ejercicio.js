'use strict'

// Tabla de multiplicar de un numero introducido por pantalla

document.writeln('Tabla de multiplicar de un numero introducido por pantalla');
document.writeln('<br><br>');

var num;

do {
    num = parseInt(prompt('Ingresa el número de la tabla de multiplicar que deseas'));
    if (isNaN(num)) {
        alert('Ingresa un numero valido');
    }
} while (isNaN(num));

document.writeln('Tabla del '+num+'<br><br>');

for(let i = 1; i <= 10; i++ ){
   document.writeln(num + ' x '+i+ '= ');
   document.writeln(num*i);
   document.writeln('<br>');

}