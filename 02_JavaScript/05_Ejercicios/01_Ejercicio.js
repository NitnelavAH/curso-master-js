'use strict'

// Programa que lee dos numeros e indica cual es mayor, cual es menor y si son iguales
document.writeln('Programa que lee dos numeros e indica cual es mayor, cual es menor y si son iguales, si los numeros son menores o igual a cero o no son validos pedir de nuevo los numeros');
document.writeln('<br><br>');
var bandera = false;

var num1;
var num2;

do {
    num1 = parseInt(prompt('Ingresa el primer número > 0'));
    num2 = parseInt(prompt('Ingresa el segundo número > 0'));
    if (num1 <= 0 || num2 <=0 || isNaN(num1) || isNaN(num2)) {
        alert('Introduce numeros validos')
    }
} while (num1 <= 0 || num2 <=0 || isNaN(num1) || isNaN(num2));

document.writeln('El primer número ingresado es: ', num1);
document.writeln('<br><br>');

document.writeln('El segundo número ingresado es: ', num2);
document.writeln('<br><br>');

if (num1 > num2) {
    document.writeln('El primer número es el mayor: ', num1);
    document.writeln('<br>');
    document.writeln('El segundo número es el menor: ', num2);
    document.writeln('<br>');
    document.writeln('No son iguales');
    
} else if(num1 < num2){
    document.writeln('El segundo número es el mayor: ', num2);
    document.writeln('<br>');
    document.writeln('El primer número es el menor: ', num1);
    document.writeln('<br>');
    document.writeln('No son iguales');
    
} else if(num1 === num2){
    document.writeln('<br>');
    document.writeln('Son iguales');
    
}
