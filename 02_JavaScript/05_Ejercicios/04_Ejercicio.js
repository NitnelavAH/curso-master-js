//Mostrar todos los numeros impares que hay entre dos numeros introducidos por el usuario

document.writeln('//Mostrar todos los numeros impares que hay entre dos numeros introducidos por el usuario');

document.writeln('<br><br>');

var num1;
var num2;

do {
    num1 = parseInt(prompt('Ingresa el primer número'));
    num2 = parseInt(prompt('Ingresa el segundo número'));
    if (isNaN(num1) || isNaN(num2)) {
        alert('ingresa numero validos');
    }
} while (isNaN(num1) || isNaN(num2));

if (num1 < num2) {
    for (let i = num1+1; i < num2; i++){

        if (i % 2 !== 0) {
            console.log(i);
            document.writeln(i);
            document.writeln('<br>');
        }
    }
} else if(num2 < num1){
    for (let i = num2+1; i < num1; i++){
        if (i % 2 !== 0) {
            console.log(i);
            document.writeln(i);
            document.writeln('<br>');
        }
    }
} else {
    console.log('son iguales');
    document.writeln('Los números introducidos son iguales');
}