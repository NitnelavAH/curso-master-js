// Utilizando un bucle, mostrar la suma y la media de los numeros introducidos hatsa introducir un numero negativo y ahí mostrar el resultado

document.writeln('Utilizando un bucle, mostrar la suma y la media de los numeros introducidos hasta introducir un numero negativo y ahí mostrar el resultado');
document.writeln('<br><br>');

var suma = 0;
var contador = 0;
var numero;

do {
    numero = parseInt(prompt('Introduce un numero'));
    if(isNaN(numero)){
        alert('Introduce un número valido');
        numero = 0;
    } else if (numero >= 0 ) {
        contador++;
        suma += numero;
        console.log(suma);
    }
    console.log(numero);
} while(numero >= 0);

document.writeln('La suma total es: ' + suma);
document.writeln('<br><br>');
document.writeln('La media es: ' + (suma / contador));