'use strict'
// Que nos diga si un numero es par o impar

document.writeln('Que nos diga si un numero es par o impar');
document.writeln('<br><br>');


var num1;

do {
    num1 = parseInt(prompt('Ingresa el número'));
    if (isNaN(num1)) {
        alert('ingresa un número valido');
    }
} while (isNaN(num1));

document.writeln('El numero ingresado es '+ num1+ '<br><br>');
if (num1 % 2 === 0) {
    document.writeln('El número es par')
}else {
    document.writeln('El número es impar')
}