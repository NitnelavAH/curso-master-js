'use strict'
 // Calculadora que pida dos numeros por pantalla, validar los numeros, mostrar la suma, resta, multiplicacion y division de esos numero

 document.writeln('Calculadora que pida dos numeros por pantalla, validar los numeros, mostrar la suma, resta, multiplicacion y division de esos numeros <br><br>');

 var num1;
 var num2;

do {
    num1 = parseInt(prompt('Ingresa un número'));
    num2 = parseInt(prompt('Ingresa un número'));
    if (isNaN(num1) || isNaN(num2)) {
        alert('ingresa numeros validos');
    }
} while (isNaN(num1) || isNaN(num2));

document.writeln('Suma de '+ num1 + ' + ' + num2 + ' = ' + (num1+num2) );
document.writeln('<br><br>');
console.log('Suma de '+ num1 + ' + ' + num2 + ' = ' + (num1+num2) );

document.writeln('Resta de '+ num1 + ' - ' + num2 + ' = ' + (num1-num2) );
document.writeln('<br><br>');
console.log('Resta de '+ num1 + ' - ' + num2 + ' = ' + (num1-num2) );

document.writeln('Multiplicación de '+ num1 + ' x ' + num2 + ' = ' + (num1*num2) );
document.writeln('<br><br>');
console.log('Multiplicación de '+ num1 + ' x ' + num2 + ' = ' + (num1*num2) );

document.writeln('División de '+ num1 + ' / ' + num2 + ' = ' + (num1/num2) );
document.writeln('<br><br>');
console.log('División de '+ num1 + ' / ' + num2 + ' = ' + (num1/num2) );

