'use strict'

// Muestre todos los numeros divisores de un numero que se introduce
document.writeln('Muestre todos los numeros divisores de un numero que se introduce');
document.writeln('<br><br>');
var num1;

do {
    num1 = parseInt(prompt('Ingresa el número'));
    if (isNaN(num1)) {
        alert('ingresa numero validos');
    }
} while (isNaN(num1));

for (let i = 0; i <= num1; i++) {
    if (num1 % i === 0) {
        console.log(i);
        document.writeln(i+' es divisor de '+num1);
        document.writeln('<br>');
    }
}