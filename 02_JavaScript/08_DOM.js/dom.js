'use strict'

// DOM Document Object Model

//SELECCIONAR ELEMENTOS POR ID

    //obtener el valor del elemento de html por su id
    var caja = document.getElementById("micaja").innerHTML;
    console.log('Este es el valor de la caja traido desde HTML con innerHTML: \n',caja);

    caja = document.getElementById("micaja").textContent;
    console.log('Este es el valor de la caja traido desde HTML con textContent: \n',caja);

    //obtener el elmento html por su id
    caja = document.getElementById("micaja");

    //cambiar el valor del elemnto html
    caja.innerHTML= "Hola soy la caja pero con el valor asignado desde JS";

    //modificar estilo del elemento html
    caja.style.background = "red";
    caja.style.color = "white";
    caja.style.padding = "20px";

    //asignar una clase al elemento html
    caja.className = "hola";

    console.log(caja);

    // QuerySelector
    //para seleccionar todo ya sea por etiqueta o clase usar querySelectorAll

    //seleccion por id
    var pId = document.querySelector("#soyP");
    pId.innerHTML = "Soy un parrafo con js";
    pId.style.background = "yellow";
    pId.style.color = "blue";
    pId.style.padding = "20px";

// SELECCIONAR ELEMENTOS POR ETIQUETA

    var lista = document.getElementsByTagName("li");
    //console.log(lista); // array elementos li
    for (let i in lista) {
        if(typeof lista[i].textContent === 'string'){
            lista[i].textContent = "soy el elemento " +(parseInt(i)+1)+" del array de li";
            // crear nuevos elementos
            let elemento = document.createElement("li");
            //crear valor de un elemento
            let textoLi = document.createTextNode('Soy el elemento '+ (parseInt(i)+1)+' de la nueva lista');
            //asignar un valor de texto a elemento
            elemento.appendChild(textoLi);
            //asignar elementos a otro elemento
            document.querySelector("#nuevaLista").appendChild(elemento);
        }
    }
   

// SELECCIONAR ELEMENTOS POR CLASE

    var titulos = document.getElementsByClassName("titulos");
    console.log(titulos);
    for(let i in titulos){
        if (typeof titulos[i] === 'object') {
            titulos[i].style.fontSize = '1.8rem';
            titulos[i].style.color = 'blue';
        }
    }
