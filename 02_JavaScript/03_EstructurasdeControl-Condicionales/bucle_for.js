'use strict'

//bucle es una estructura de control que se repite varias veces
document.write('Tablas con FOR ');
    document.write("<br><br>");
for (let i = 1; i <= 10; i++) {
    document.write('TABLA DEL ', i);
    document.write("<br>");
    console.log('TABLA DEL ', i);
    for(let j = 1; j <= 10; j++) {
        document.writeln(i,' x ',j, ' = ', i*j);
        document.write("<br>");
        console.log(i,' x ',j, ' = ', i*j);
    }
    document.write("<br>");
    
}