// Bucle while

document.write('Tablas con WHILE ');
    document.write("<br><br>");

var i = 1;
var j = 1;

while (i <= 10) {

    document.write('TABLA DEL ', i);
    document.write("<br>");
    console.log('TABLA DEL ', i);
    j = 1;
    while (j <= 10) {
        document.writeln(i,' x ',j, ' = ', i*j);
        document.write("<br>");
        console.log(i,' x ',j, ' = ', i*j);
        j++;
    }
    document.write("<br>");
    i++;
}