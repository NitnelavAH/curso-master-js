'use strict' //uso estricto

/* 
    Operadores Relacionales
    Mayor >
    Menor <
    Mayor o igual >=
    Menor o igual <=
    Igual == o ===
    Distinto != o !==



    Los operadores === y !== son los operadores de comparación estricta. Esto significa que si los operandos tienen tipos diferentes, no son iguales. Por ejemplo,

    1 === "1" // false
    1 !== "1"  // true
    null === undefined // false
    Los operadores == y != son los operadores de comparación relajada. Es decir, si los operandos tienen tipos diferentes, JavaScript trata de convertirlos para que fueran comparables. Por ejemplo,

    1 == "1" // true
    1 != "1" // false
    null == undefined // true
*/

// Condicional if si A es iagual a B haz esto si no haz esto otro
const A = 10;
const B = 20;

if (A === B) {
    console.log('son iguales');
} else if (A <= 10) {
    console.log('A es menor o igual a 10');
} else{
    console.log('hola mundo');
} 

/* Operadores Logicos
    AND &&
    OR ||
    NOT !
*/

//And
if (A === 10 && B === 20) {
    console.log('A es 10 y B es 20');
}

//OR
if (A === 1 || B === 20) {
    console.log('A es 10 o B es 20');
}

//NOT
if(!A===10){
    console.log('A es 10');
} else {
    console.log('A no es 10');
}

//Combinacion
if(A === 10 && (A >= 10 && B <= 20)){
    console.log('se cumple lo del parentesis');
} else {
    console.log('no se cumple lo del parentesis');
}