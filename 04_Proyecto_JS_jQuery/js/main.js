'use strict'
$(document).ready(function() {
    console.log('jquery ready');

  
    
    //SLIDER
    if(window.location.href.indexOf('index') > -1){ //Verificar si esta en la pagina index
        $('.bxslider').bxSlider({
            mode: 'fade',
            captions: true,
            responsive: true,
            pager: true,
            auto: true,
            speed: 2000
        });
    

    //POSTS
    
    
        var posts = [
            {
                title: 'Pruba Titulo 1',
                date: 'Publicado el ' + moment().date() + ' de '+ moment().format('MMMM') + ' del '+ moment().format("YYYY"),
                content: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis, voluptatum. Veniam numquam eaque, doloremque voluptas tenetur eius, sint, assumenda nam dignissimos recusandae repudiandae rem magnam aliquid quidem distinctio blanditiis repellendus? Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus voluptates excepturi quam praesentium ullam! Facilis, nihil iusto? Itaque iure deleniti illum cum impedit quia? Neque corrupti pariatur ratione eos natus.',
                
            },
            {
                title: 'Pruba Titulo 2',
                date: 'Publicado el ' + moment().date() + ' de '+ moment().format('MMMM') + ' del '+ moment().format("YYYY"),
                content: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis, voluptatum. Veniam numquam eaque, doloremque voluptas tenetur eius, sint, assumenda nam dignissimos recusandae repudiandae rem magnam aliquid quidem distinctio blanditiis repellendus? Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus voluptates excepturi quam praesentium ullam! Facilis, nihil iusto? Itaque iure deleniti illum cum impedit quia? Neque corrupti pariatur ratione eos natus.',
                
            },
            {
                title: 'Pruba Titulo 3',
                date: 'Publicado el ' + moment().date() + ' de '+ moment().format('MMMM') + ' del '+ moment().format("YYYY"),
                content: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis, voluptatum. Veniam numquam eaque, doloremque voluptas tenetur eius, sint, assumenda nam dignissimos recusandae repudiandae rem magnam aliquid quidem distinctio blanditiis repellendus? Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus voluptates excepturi quam praesentium ullam! Facilis, nihil iusto? Itaque iure deleniti illum cum impedit quia? Neque corrupti pariatur ratione eos natus.',
                
            },
            {
                title: 'Pruba Titulo 4',
                date: 'Publicado el ' + moment().date() + ' de '+ moment().format('MMMM') + ' del '+ moment().format("YYYY"),
                content: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis, voluptatum. Veniam numquam eaque, doloremque voluptas tenetur eius, sint, assumenda nam dignissimos recusandae repudiandae rem magnam aliquid quidem distinctio blanditiis repellendus? Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus voluptates excepturi quam praesentium ullam! Facilis, nihil iusto? Itaque iure deleniti illum cum impedit quia? Neque corrupti pariatur ratione eos natus.',
                
            },
            {
                title: 'Pruba Titulo 5',
                date: 'Publicado el ' + moment().date() + ' de '+ moment().format('MMMM') + ' del '+ moment().format("YYYY"),
                content: 'Lorem ipsum dolor, sit amet consectetur adipisicing elit. Reiciendis, voluptatum. Veniam numquam eaque, doloremque voluptas tenetur eius, sint, assumenda nam dignissimos recusandae repudiandae rem magnam aliquid quidem distinctio blanditiis repellendus? Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus voluptates excepturi quam praesentium ullam! Facilis, nihil iusto? Itaque iure deleniti illum cum impedit quia? Neque corrupti pariatur ratione eos natus.',
                
            },
        ];

    

    // console.log(posts);

        posts.forEach((item, index) => {
            var post = `
                <article class="post">
                    <h2>${item.title}</h2>
                    <span class="fecha">${item.date}</span>
                    <p>${item.content}</p>
                    <a href="" class="button-more">Leer mas</a>
                </article>
            `;
            $("#posts").append(post);
        });
    }
    // SELECTOR DE TEMA
    
    var theme = $('#theme');
    var themeStorage = localStorage.getItem('theme');
    if (themeStorage) {
       changeTheme(themeStorage);
    } else {
        console.log('No Hay Storage');
    }
    
    $("#to-green").click(function() {
        changeTheme("/Proyecto_JS_jQuery/css/green.css");
    });
    
    $("#to-red").click(function() {
        changeTheme("/Proyecto_JS_jQuery/css/red.css");
    });
    
    $("#to-blue").click(function() {
        changeTheme("/Proyecto_JS_jQuery/css/blue.css");
    });
    
    function changeTheme(url){
        theme.attr("href", url);
        localStorage.setItem('theme', url);
    }


    //Scroll arriba de la web
    $('.subir').click(function(e){
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1200);

        return false;
    });

    //Login Falso
    $("#login form").submit(function(){
        let form_name = $('#name').val();

        localStorage.setItem('name', form_name);

        
    });

    var name = localStorage.getItem("name");
    // console.log(name);
    if(name) {
        let parrafo = $("#about p");
        parrafo.html("<br><strong>Bienvenido, "+ name+"</strong>");

        parrafo.append("<a href='#' id='logout'>Cerrar Sesión</a>");

        $('#login').hide();

        $("#logout").click(function(){
            localStorage.removeItem('name');
            location.reload();
        });
    }

    //Acordeon
    if(window.location.href.indexOf('about') > -1){
        $('#acordeon').accordion();
    }

    //Reloj
    if(window.location.href.indexOf('reloj') > -1){
         setInterval(function(){
            var reloj = moment().format("hh:mm:ss");
            $('#reloj').html(reloj);
         }, 1000)
    }


    //Validacion
    if(window.location.href.indexOf('contact') > -1){

        $("form input[name='date']").datepicker({
			dateFormat: 'dd-mm-yy'
        });
        
        $.validate({
            lang: 'es',
            errorMessagePosition: 'top',
		    scrollToTopOnError: true
        });
   }
});
