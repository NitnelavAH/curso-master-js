//Interface

interface CamisetaBase{
    setColor(color: string);
    getColor();
}


//Clase (molde del objeto)
class Camiseta implements CamisetaBase{
    //Propiedades (caracteristicas del objeto)
    private color: string;
    private modelo: string;
    private marca: string;
    private talla: string;
    private precio: number;
    
    
    //Métodos (funciones o acciones del objeto)
    constructor(color:string, modelo:string, marca:string, talla:string, precio:number){
        this.color = color;
        this.modelo = modelo;
        this.marca = marca;
        this.talla = talla;
        this.precio = precio;
    }

    public setColor(color: string){
        this.color = color
    }

    public getColor(){
        return this.color;
    }

}

var camiseta = new Camiseta('blanco', 'polo', 'AE', 'M', 199);

camiseta.setColor('Rojo');

console.log(camiseta);
console.log(camiseta.getColor());




//Clase hija

class Sudadera extends Camiseta{
    public capucha: boolean;

    setCapucha(capucha: boolean){
        this.capucha = capucha;
    }

    getCapucha(){
        return this.capucha;
    }
}

var sudadera = new Sudadera('blanco', 'polo', 'AE', 'M', 199);

sudadera.setCapucha(true);
sudadera.setColor('Azul');
console.log(sudadera);