import { Camiseta } from './05-clases-interfaces/camiseta';


class Main{
    constructor(){
        console.log('aplicacion JS cargada!!');
    }
}

var main = new Main();

var camiseta = new Camiseta('blanco', 'polo', 'AE', 'M', 199);

camiseta.setColor('Rojo');

console.log(camiseta);
console.log(camiseta.getColor());