//string
var cadena = "NITNELAVAH";
console.log(cadena);
//number
var minumero = 666;
console.log(minumero);
//boolean
var booleano = true;
console.log('am i?', booleano);
//any
var cualquiera = 0.5453334223442234;
console.log(cualquiera);
//Arrays
var lenguajes = ['JS', 'PHP', 'JAVA'];
console.log(lenguajes);
var anios = [1992, 1993, 1997, 2001];
console.log(anios);
//varios tipos
var varios = "soy un string";
console.log(varios);
varios = 12;
console.log('Ahora soy un número', varios);
var mitipodedato = true;
console.log('mi tipo de datos es', mitipodedato);
