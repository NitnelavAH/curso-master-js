//string

let cadena: string = "NITNELAVAH";
console.log(cadena);

//number
let minumero: number = 666;
console.log(minumero);

//boolean
let booleano: boolean = true;
console.log('am i?', booleano);

//any
let cualquiera: any = 0.5453334223442234
console.log(cualquiera);

//Arrays
var lenguajes: Array<string> = ['JS', 'PHP', 'JAVA'];
console.log(lenguajes);

let anios: number[] = [1992, 1993,1997,2001];
console.log(anios);

//varios tipos
let varios: string | number = "soy un string";
console.log(varios);
varios = 12;
console.log('Ahora soy un número', varios);

//definir tipo de datos
type miTipoDeDatos = number | boolean;

let mitipodedato: miTipoDeDatos = true;

console.log('mi tipo de datos es', mitipodedato);