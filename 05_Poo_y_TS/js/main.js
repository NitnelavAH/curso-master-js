"use strict";
exports.__esModule = true;
var camiseta_1 = require("./05-clases-interfaces/camiseta");
var Main = /** @class */ (function () {
    function Main() {
        console.log('aplicacion JS cargada!!');
    }
    return Main;
}());
var main = new Main();
var camiseta = new camiseta_1.Camiseta('blanco', 'polo', 'AE', 'M', 199);
camiseta.setColor('Rojo');
console.log(camiseta);
console.log(camiseta.getColor());
