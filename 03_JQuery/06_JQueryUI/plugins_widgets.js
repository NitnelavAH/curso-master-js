'use strict'

$(document).ready(function () {
    console.log('jquery listo');

    //TOOTLTIP
     $('a').tooltip();

    //DIALOG
    $('#popup').click(function(){
        $('#dialogo').dialog();
    });

    //CALENDARIO datepicker
    $('#calendario').datepicker();

    //TABS
    $('#pestanas').tabs();
});