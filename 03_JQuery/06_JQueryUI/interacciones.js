'use strict'

$(document).ready(function(){
    console.log('jquery listo');

    var elemento = $('.elemento');


    //MOVER ELEMENTO POR LA PAGINA
    elemento.draggable();

    //REDIMENSIONAR
    elemento.resizable();

    // Seleccionar elementos
    $('#seleccionar').selectable();

    // Ordenar Elementos
    $('#ordenar').sortable({
        update: function(event, ui){
            console.log('cambio la lista');
        }
    });

    //DROP
    $('#elemento-movido').draggable();
    $('#area').droppable({
        drop: function(){
            console.log('Has soltado algo dentro de el area verde');
        }
    });

    //Efectos

    $('#mostrar').click(function(){
        // $('.caja-efectos').toggle('fade');
        // $('.caja-efectos').toggle('explode');
        // $('.caja-efectos').toggle('blind');
        // $('.caja-efectos').toggle('slide');
        // $('.caja-efectos').toggle('drop');
        // $('.caja-efectos').toggle('puff');
        // $('.caja-efectos').toggle('scale');
        // $('.caja-efectos').toggle('shake');
        $('.caja-efectos').toggle('shake', 500);

    });
});