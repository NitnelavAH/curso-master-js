'use strict'

$(document).ready(() => { // el simbolo $ es equivalente a usas jQuery
    console.log('jquery listo');

    //selector por ID
    var rojo = $("#rojo").css("background", "red")
                         .css("color", "white");
    console.log(rojo);

    $("#amarillo").css("background", "yellow")
                  .css("color","red");

    $("#verde").css("background", "green")
               .css("color","yellow");

    $("#azul").css("background", "blue")
              .css("color", "white");


    //selector por clase
    var mi_clase = $('.zebra');
    console.log(mi_clase[0]);
    console.log(mi_clase.eq(1));

    mi_clase.css('padding', '5px');

    $('.sin_borde').click(function(){
        console.log('click');
        $(this).addClass('zebra');
    });

    //selectores de etiqueta
    var parrafos = $('p').css("cursor", "pointer");

    parrafos.click(function() {
        let p = $(this);
        if (p.hasClass('grande')) {
            p.removeClass('grande');
        } else {
            p.addClass('grande');
        }
    });

    // selectores de atributos
   $('[title="udemy"]').css('background', '#cccccc');
   $('[title="youtube"]').css('background', 'red');


   //otros
   $('p,a').addClass('margenSuperior');//selecciona las etiquetas p y a

   var busqueda = $("#caja").find('.resaltado'); //busca los elementos dentro de caja que contienen la clase resaltado que es equivalente a usar $("#caja .resaltado"); BUSCAR RAMAS EN EL ARBOL
   console.log(busqueda);
    //SUBIR DE RAMA USAR EL METODO .parent()
    var parent = $('#caja .resaltado').eq(0).parent().parent().parent().find('[title="udemy"]');
    console.log('parent',parent);
    


   

});