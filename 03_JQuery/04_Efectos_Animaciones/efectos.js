'use strict'

$(document).ready(()=>{
    console.log('jquery listo');

    var mostrar = $('#mostrar');
    var ocultar = $('#ocultar');
    var togglee = $('#toggle');
    var caja = $('#caja');
    var caja2 = $('#caja2');

    mostrar.hide();

    mostrar.click(function(){
        // caja.show('fast');
        // caja.fadeIn('normal');
        // caja.fadeTo('normal',0.9);
        caja.slideDown('slow');
        mostrar.hide(1000);
        ocultar.show(500);
    });

    ocultar.click(() =>{
        // caja.hide('fast');
        // caja.fadeOut('normal');
        // caja.fadeTo('normal',0.1);
        caja.slideUp('fast');
        ocultar.hide(1000);
        mostrar.show(500);
    });

    togglee.click(()=>{
        // caja2.toggle(500);
        caja2.slideToggle('slow');
    });

    var animar = $('#animame');
    var caja3 = $('#caja3');
    animar.click(()=>{
        caja3.animate(
            {
                marginLeft: '500px',
                fontSize: '40px',
                height: '110px',
                width: '350px'
            }, 'slow')
        .animate(
            {
            borderRadius: '900px',
            marginTop: '90px'
        }, 'slow')
        .animate(
            {
                borderRadius: '0px',
                marginTop: '0px'
            }, 'slow'
        );
    });



});