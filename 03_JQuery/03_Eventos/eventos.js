'use strict'
$(document).ready(()=>{
    console.log('jquery listo');

    //MouseOver y MoudeOut
    var caja = $('#caja1');

    caja.mouseover(function () { 
        $(this).css("background", "green");
    });

    caja.mouseout(function () { 
        $(this).css("background", "yellow");
    });


    // Hover
    var caja2 = $("#caja2");
    caja2.hover(function () {
            // over
            $(this).css("background", "green");
        }, function () {
            // out
            $(this).css("background", "yellow");
        }
    );

    // Click y Doble Click
    var caja3 = $('#caja3');
    caja3.click(function(){
        $(this).css("background", "blue")
               .css("border","2px solid red");
    });

    caja3.dblclick(function(){
        $(this).css("background", "yellow")
               .css("border","2px solid black");
    });


    // blur y focus
    var nombre = $('#nombre');
    nombre.focus(function(){
        $(this).css('border', '2px solid blue');
    });

    var datos = $('#datos');

    nombre.blur(function(){
        let nom = $(this);
        nom.css('border', '2px solid green');
        if(nom.val().length > 0){
            datos.text($(this).val()).show();
        }else{
            datos.hide();
        }

       
    });

    //Mousedown y Mouseup

    datos.mousedown(function(){
        $(this).css("border-color", "red");
    });

    datos.mouseup(function(){
        $(this).css("border-color", "black");
    });

    //Mousemove
    $(document).mousemove(function() {
        //console.log('x',event.clientX,',y',event.clientY); 
        $('body').css('cursor','none');
        $('#sigueme').css('left',event.clientX).css('top', event.clientY);
    });

});


