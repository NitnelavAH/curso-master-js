'use strict'

// https://reqres.in/

$(document).ready(()=>{
    console.log('jQuery Listo');

    // Load
    // $('#datos').load('https://reqres.in/');//carga todo el contenido obtenido por get en el elemento 

    // Get y Post

    $.get('https://reqres.in/api/users', {page: 2},
        function (resp, status) {
            console.log('GET',resp, status);
            resp.data.forEach(element => {
                $('#datos').append('<p>'+element.first_name+' '+ element.last_name+'</p>')
            });
        }
    );


    $('#formulario').submit(function(e){
        e.preventDefault();
        let user = {
            name: $('input[name="nom"]').val(),
            web: $('#web').val()
        }

        console.log('se creo',user);

    $.post($(this).attr('action'), user,
        function (resp, status) {
            console.log('POST',resp,status);
        }).done( function(){
            console.log('Usuario creado exitosamente');
        });

        return false;
    });


    // AJAX
    $('#formulario2'). submit(function(e){
        e.preventDefault();

        let user = {
            name: $('#nombre2').val(),
            webSite: $('#web2').val()
        }
        $.ajax({
            type: 'POST',
            url: $(this).attr('action'),
            data: user,
            beforeSend: function(){
                console.log('Enviando...');
            },
            success: function(res, status){
                console.log(res, status);
            },
            error: function(){
                console.log('Ocurrio un error, se acabo el tiempo de espera');
            },
            timeout: 1500
        });

        return false;
    });



});