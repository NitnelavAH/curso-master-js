import { Component } from '@angular/core';
import { Configuracion } from './models/configuracion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  oculto: boolean = false;
  public config;

  constructor(){
    this.config = Configuracion;
  }

  ocultarVideojuegos(value){
    
    this.oculto = value;
  }
}
