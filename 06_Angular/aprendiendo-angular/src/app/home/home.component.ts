import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  identificado: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  setIdentificado() {
    this.identificado = true;
  }

  cerrarSesion() {
    this.identificado = false;
  }

}
