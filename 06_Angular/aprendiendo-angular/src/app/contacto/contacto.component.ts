import { Component, OnInit } from '@angular/core';
import { ContactoUsuario } from '../models/contactoUsuario';


@Component({
  selector: 'app-contacto',
  templateUrl: './contacto.component.html',
  styleUrls: ['./contacto.component.scss']
})
export class ContactoComponent implements OnInit {

  usuario: ContactoUsuario;

  constructor() {
    this.usuario = new ContactoUsuario('', '', '', '');
  }

  ngOnInit(): void {
  }

  onSubmit(form) {
    console.log('Submit Lanzado');
    console.log(this.usuario);
    form.reset();
  }

}
