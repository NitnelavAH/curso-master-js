import { Component, OnInit } from '@angular/core';
import { Zapatilla } from '../models/zapatilla';
import { ZapatillasService } from '../services/zapatillas.service';

@Component({
  selector: 'app-zapatillas',
  templateUrl: './zapatillas.component.html',
  styleUrls: ['./zapatillas.component.scss']
})
export class ZapatillasComponent implements OnInit {

  titulo = ' Componente de Tenis chidos';
  zapatillas: Zapatilla[] = [];
  marcas: string[] = [];
  color = 'red';
  miMarca: string;

  constructor(private zapatillasService: ZapatillasService) {

  }

  ngOnInit(): void {

    this.getMarcas();
    this.zapatillas = this.zapatillasService.getZapatillas();
    alert(this.zapatillasService.getTexto());
  }

  getMarcas() {
    this.zapatillas.forEach(element => {
      if (this.marcas.indexOf(element.marca) < 0) {
        this.marcas.push(element.marca);
      }
    });

    console.log(this.marcas);
  }

  getMarca() {
    if (this.miMarca) {
      alert(this.miMarca);
    }
  }

  addMarca() {
    if (this.miMarca) {
      this.marcas.push(this.miMarca);
    }
  }

  borrarMarca(index: number) {
    //delete this.marcas[index];
    this.marcas.splice(index, 1);
  }

  onBlur() {
    console.log('haz salido del input');
  }

}
