import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeticionesService {

  URL = 'https://reqres.in/';
  constructor(
    private http: HttpClient,

  ) { }

  getUser(userId: number): Observable<any> {
    return this.http.get(this.URL + 'api/users/' + userId);
  }

  addUser(user): Observable<any> {
    let params = JSON.stringify(user);
    let headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.post(this.URL + 'api/users', params, { headers: headers });
  }
}
