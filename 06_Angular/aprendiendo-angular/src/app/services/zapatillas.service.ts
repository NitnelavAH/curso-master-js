import { Injectable } from '@angular/core';
import { Zapatilla } from '../models/zapatilla';

@Injectable({
  providedIn: 'root'
})
export class ZapatillasService {

  public zapatillas: Zapatilla[] = [];

  constructor() {
    this.zapatillas.push(
      new Zapatilla('Rebook Classic', 599, 'Rebook', 'blanco', true),
      new Zapatilla('Nike Runner', 539, 'Nike', 'azul', true),
      new Zapatilla('Adidas Classic', 499, 'Adidas', 'blanco', true),
      new Zapatilla('Adidas Samba', 399, 'Adidas', 'rojo', true),
      new Zapatilla('Adidas Reggae', 299, 'Adidas', 'azul', false)
    )
  }

  getTexto() {
    return 'Hol Mundo desde un servicio';
  }

  getZapatillas(): Array<Zapatilla> {
    return this.zapatillas;
  }
}
