import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.scss']
})
export class CursosComponent implements OnInit {

  nombre: string = null;
  followers: number;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute

  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.nombre = params.nombre;
      this.followers = + params.followers;
      console.log(params);
    });
  }

  redirigir() {
    this.router.navigate(['/zapatillas']);
  }

}
