export class Zapatilla{
    /* public nombre: string;
    public precio: number;
    public marca: string;
    public color: string;
    public stock: boolean; */

    constructor(
        public nombre: string, 
        public precio: number, 
        public marca: string, 
        public color: string, 
        public stock:boolean
    ) {
    }
}