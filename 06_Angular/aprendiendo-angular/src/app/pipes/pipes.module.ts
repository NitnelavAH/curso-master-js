import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalculadoraPipe } from './calculadora.pipe';



@NgModule({
  declarations: [
    CalculadoraPipe
  ],
  exports: [
    CalculadoraPipe
  ],
  imports: [
    CommonModule,
  ],
})
export class PipesModule { }
