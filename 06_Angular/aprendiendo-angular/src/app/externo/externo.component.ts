import { Component, OnInit } from '@angular/core';
import { PeticionesService } from '../services/peticiones.service';

@Component({
  selector: 'app-externo',
  templateUrl: './externo.component.html',
  styleUrls: ['./externo.component.scss']
})
export class ExternoComponent implements OnInit {

  user: any;
  userId: number;
  fecha: Date;

  new_user: any = {
    "name": "",
    "job": ""
  };
  constructor(
    private peticionesService: PeticionesService
  ) { }

  ngOnInit(): void {
    this.userId = 2;
    this.fecha = new Date();
    this.cargaUsuario();
  }

  cargaUsuario() {
    this.user = false;
    this.peticionesService.getUser(this.userId).subscribe(
      result => {
        console.log(result);
        this.user = result.data;
      },
      error => {
        console.log(error);
      }
    );
  }

  onSumbit(form) {
    this.peticionesService.addUser(this.new_user).subscribe(
      res => {
        console.log(res);
        form.reset();
      }, err => {
        console.log(err);
      }
    );

  }

}
