import { Component, DoCheck, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-videojuego',
  templateUrl: './videojuego.component.html',
  styleUrls: ['./videojuego.component.scss']
})
export class VideojuegoComponent implements OnInit, DoCheck, OnDestroy {

  titulo: string;

  constructor() {
    this.titulo = 'Componente de Videojuegos';
   }

  ngOnInit(): void {
    console.log('OnInit Ejecutado');
  }

  ngDoCheck(){
    console.log('DoCheck Ejecutado');
  }

  ngOnDestroy(){
    console.log('OnDestroy Ejecutado');
  }

}
